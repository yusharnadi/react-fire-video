import React from "react";
import "./App.css";
import firebase from './firebase'
function App() {
  const [play, setPlay] = React.useState(false);
  let vidRef = React.useRef(null);

  React.useEffect(()=>{
    // let elem = document.querySelector("html");
    // elem.requestFullscreen().catch(err => {console.log('error waass')})
    const db = firebase.firestore().collection('button').doc('1');
    db.onSnapshot((doc) => {
      //console.log(doc.data().val)
      setPlay(doc.data().val)
    })
  }, [])

  if(play){
    console.log(vidRef)
    vidRef.current.play()
  }

  return (
    <div className="app">
      <video ref={vidRef}>
        <source src="count.mp4" type="video/mp4" />
      </video>
    </div>
  );
}

export default App;
