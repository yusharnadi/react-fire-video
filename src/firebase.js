import firebase from 'firebase';
import 'firebase/firestore';
const firebaseConfig = {
    apiKey: "AIzaSyB5fgUERGV3akuRDHhFywDjxDjhkonWiwM",
    authDomain: "tcm-launching.firebaseapp.com",
    databaseURL: "https://tcm-launching.firebaseio.com",
    projectId: "tcm-launching",
    storageBucket: "tcm-launching.appspot.com",
    messagingSenderId: "982870816484",
    appId: "1:982870816484:web:0da9487ba0c03efc9c9ef1",
    measurementId: "G-WZE5NM7BG8"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  // firebase.analytics();
  export default firebase;
